<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Referencias Bootstrap -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">

    <!-- Conexión -->
    <?php require_once "conexion.php"; ?>
    <link rel="stylesheet" href="conexion.php">

    <title>Resultados COA</title>
</head>
<!-- sadsadsa -->
<body class="body-index">
    <div class="container">
        <div class="row content">
            <div class="col-md-6 mb-3">
                <img src="img/img.jpeg" alt="img-fluid" alt="image" class="img-fluid imagen">
            </div>
            <div class="col-md-6">
                <h3 class="signin-text mb-3">Iniciar Sesión</h3>
                <form method="post" action="">
                    <div id="div_login">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="txt_uname" class="form-control" id="txt_uname">
                        </div>
                        <div class="form-group">
                            <label for="password">Contraseña</label>
                            <input type="password" name="txt_pwd" class="form-control" id="txt_uname">
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" name="checkbox" class="form-check-input" id="checkbox">
                            <label class="form-check-label" for="checkbox">Recordar</label>
                        </div>
                        <div>
                            <input class="btn boton" type="submit" value="Iniciar Sesión" name="but_submit" id="but_submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>

    <?php
        include "config.php";

        if(isset($_POST['but_submit'])){

            $uname = mysqli_real_escape_string($con,$_POST['txt_uname']);
            $password = mysqli_real_escape_string($con,$_POST['txt_pwd']);

            if ($uname != "" && $password != ""){

                $sql_query = "select count(*) as cntUser from encargado where email='".$uname."' and clave='".$password."'";
                $result = mysqli_query($con,$sql_query);
                $row = mysqli_fetch_array($result);

                $count = $row['cntUser'];

                if($count > 0){
                    $_SESSION['uname'] = $uname;
                    header('Location: home.php');
                }else{
                    echo "Invalid username and password";
                }

            }
            
        }
    ?>

    
</body>
</html>