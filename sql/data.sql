-- MySQL dump 10.13  Distrib 8.0.17, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: WebEducacional
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alumno` (
  `idalumno` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cf_idcurso` int(11) NOT NULL,
  `numero_lista` int(11) DEFAULT NULL,
  `rut` varchar(12) DEFAULT NULL,
  `nombre` varchar(40) DEFAULT NULL,
  `apellidos` varchar(40) DEFAULT NULL,
  `genero` varchar(9) DEFAULT NULL,
  `nee` enum('SI','NO') DEFAULT NULL,
  `ingreso` enum('Antiguo','Nuevo') DEFAULT NULL,
  PRIMARY KEY (`idalumno`),
  UNIQUE KEY `rut_UNIQUE` (`rut`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
INSERT INTO `alumno` VALUES (1,1,1,NULL,'ANTONIA','ACEITÓN DURÁN','FEMENINO','NO','Nuevo'),(2,1,2,NULL,'MARÍA','ACEVEDO VON DER BECKE KLUCHTZNER','FEMENINO','NO','Nuevo'),(3,1,3,NULL,'MÁXIMO','ACUÑA RIVERA','MASCULINO','NO','Nuevo'),(4,1,4,NULL,'FLORENCIA','ALCARRAZ PEÑA','FEMENINO','NO','Antiguo'),(5,1,5,NULL,'MARTÍN','BARRERA ZAMBRANO','MASCULINO','NO','Antiguo'),(6,1,6,NULL,'MEI','BUSTAMANTE GÓMEZ','FEMENINO','NO','Antiguo'),(7,1,7,NULL,'SOFÍA','CABEZAS GONZÁLEZ','FEMENINO','NO','Antiguo'),(8,1,8,NULL,'GRETCHEN','CACHIQUE FLORES','FEMENINO','NO','Antiguo'),(9,1,9,NULL,'EMILIA','CARRILLO HERRER','FEMENINO','NO','Antiguo'),(10,1,10,NULL,'ANTONIA','CAULLÁN SEPÚLVEDA','FEMENINO','NO','Antiguo'),(11,1,11,NULL,'AMANDA','DE LA BARRA NEDER','FEMENINO','NO','Antiguo'),(12,1,12,NULL,'MARÍA PAZ','FAJARDO CELIS','FEMENINO','NO','Antiguo'),(13,1,13,NULL,'AGUSTÍN','GANA GARAY','MASCULINO','NO','Antiguo'),(14,1,14,NULL,'LUCIANO','GUERRA GARCÍA','MASCULINO','NO','Antiguo'),(15,1,15,NULL,'RENATA','JORGE HORTA','FEMENINO','NO','Antiguo'),(16,1,16,NULL,'LEÓN','JORQUERA SÁNCHEZ','MASCULINO','NO','Antiguo'),(17,1,17,NULL,'VALENTINA','LOBOS OLATE','FEMENINO','NO','Antiguo'),(18,1,18,NULL,'MATEO','NARVÁEZ POBLETE','MASCULINO','NO','Antiguo'),(19,1,19,NULL,'EMILIA','NUÑEZ LAPOSTOL','FEMENINO','NO','Antiguo'),(20,1,20,NULL,'AGUSTÍN','PEREIRA PERALTA','MASCULINO','NO','Antiguo'),(21,1,21,NULL,'BENJAMÍN','PÉREZ FUENZALIDA','MASCULINO','NO','Antiguo'),(22,1,22,NULL,'SEBASTIÁN','PETIT PUMARINO','MASCULINO','NO','Antiguo'),(23,1,23,NULL,'MARIANA','ROJAS QUEVEDO','FEMENINO','NO','Nuevo'),(24,1,24,NULL,'ISABELLA','ROSELLO SERRA','FEMENINO','NO','Nuevo'),(25,1,25,NULL,'FERNANDA','RUZ OLMEDO','FEMENINO','NO','Nuevo'),(26,1,26,NULL,'ALEJANDRO','SALAZAR MUÑOZ','MASCULINO','NO','Antiguo'),(27,1,27,NULL,'LAURA','TAPIA UGAS','FEMENINO','NO','Antiguo'),(28,1,28,NULL,'AGUSTÍN','VALDEBENITO ALTAMIRANO','MASCULINO','NO','Antiguo'),(29,1,29,NULL,'VICENTE','VALENZUELA CAMPOS','MASCULINO','NO','Nuevo'),(30,1,30,NULL,'HÉCTOR','VERGARA SERRANO','MASCULINO','NO','Antiguo'),(31,2,1,NULL,'ANTONIA','ACEITÓN DURÁN','FEMENINO','NO','Antiguo'),(32,2,2,NULL,'MARÍA','ACEVEDO VON DER BECKE KLUCHTZNER','FEMENINO','NO','Antiguo'),(33,2,3,NULL,'MÁXIMO','ACUÑA RIVERA','MASCULINO','NO','Antiguo'),(34,2,4,NULL,'FLORENCIA','ALCARRAZ PEÑA','FEMENINO','NO','Antiguo'),(35,2,5,NULL,'MARTÍN','BARRERA ZAMBRANO','MASCULINO','NO','Antiguo'),(36,2,6,NULL,'MEI','BUSTAMANTE GÓMEZ','FEMENINO','NO','Antiguo'),(37,2,7,NULL,'SOFÍA','CABEZAS GONZÁLEZ','FEMENINO','NO','Antiguo'),(38,2,8,NULL,'GRETCHEN','CACHIQUE FLORES','FEMENINO','NO','Antiguo'),(39,2,9,NULL,'EMILIA','CARRILLO HERRER','FEMENINO','NO','Antiguo'),(40,2,10,NULL,'ANTONIA','CAULLÁN SEPÚLVEDA','FEMENINO','NO','Antiguo'),(41,2,11,NULL,'AMANDA','DE LA BARRA NEDER','FEMENINO','NO','Antiguo'),(42,2,12,NULL,'MARÍA PAZ','FAJARDO CELIS','FEMENINO','NO','Antiguo'),(43,2,13,NULL,'AGUSTÍN','GANA GARAY','MASCULINO','NO','Antiguo'),(44,2,14,NULL,'LUCIANO','GUERRA GARCÍA','MASCULINO','NO','Antiguo'),(45,2,15,NULL,'RENATA','JORGE HORTA','FEMENINO','NO','Antiguo'),(46,2,16,NULL,'LEÓN','JORQUERA SÁNCHEZ','MASCULINO','NO','Antiguo'),(47,2,17,NULL,'VALENTINA','LOBOS OLATE','FEMENINO','NO','Antiguo'),(48,2,18,NULL,'MATEO','NARVÁEZ POBLETE','MASCULINO','NO','Antiguo'),(49,2,19,NULL,'EMILIA','NUÑEZ LAPOSTOL','FEMENINO','NO','Antiguo'),(50,2,20,NULL,'AGUSTÍN','PEREIRA PERALTA','MASCULINO','NO','Antiguo'),(51,2,21,NULL,'JUAN','GÓMEZ VENEGAS','MASCULINO','NO','Nuevo'),(52,2,22,NULL,'MATEO','DE TORO Y ZAMBRANO','MASCULINO','NO','Nuevo'),(53,2,23,NULL,'BELLA','SEPULVEDA ZAMBRANO','FEMENINO','NO','Nuevo'),(54,2,24,NULL,'ANTONIA','SEPULVEDA ZAMBRANO','FEMENINO','NO','Nuevo'),(55,2,25,NULL,'JOAQUIN','VEJAR RIQUELME','MASCULINO','NO','Nuevo');
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aplica`
--

DROP TABLE IF EXISTS `aplica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aplica` (
  `cf_idcurso` int(11) NOT NULL,
  `cf_idevaluacion` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `promedio` float DEFAULT NULL,
  `ptje_simce` int(11) DEFAULT NULL,
  `ptje_ptu` int(11) DEFAULT NULL,
  `nota_min` float DEFAULT NULL,
  `nota_max` float DEFAULT NULL,
  `desviacion_estandar` float DEFAULT NULL,
  `porcentaje_insuficientes` float DEFAULT NULL,
  PRIMARY KEY (`cf_idevaluacion`,`cf_idcurso`),
  KEY `fk_alumno_has_evaluacion_evaluacion1_idx` (`cf_idevaluacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aplica`
--

LOCK TABLES `aplica` WRITE;
/*!40000 ALTER TABLE `aplica` DISABLE KEYS */;
INSERT INTO `aplica` VALUES (1,1,'2020-05-01',6.6,365,589,4.3,6.3,2.86,0),(1,2,'2020-07-01',6.7,400,600,4.5,6.5,2.54,0),(1,3,'2020-09-01',6,450,650,4.8,7,1.4,0);
/*!40000 ALTER TABLE `aplica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colegio`
--

DROP TABLE IF EXISTS `colegio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `colegio` (
  `idcolegio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cf_idencargado` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `teléfono` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  `region` varchar(50) DEFAULT NULL,
  `tipo_colegio` enum('PARTICULAR','PARTICULAR SUBVENCIONADO','MUNICIPAL') DEFAULT NULL,
  PRIMARY KEY (`idcolegio`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colegio`
--

LOCK TABLES `colegio` WRITE;
/*!40000 ALTER TABLE `colegio` DISABLE KEYS */;
INSERT INTO `colegio` VALUES (1,1,'Santa Teresita',363636,'santateresita@gmail.com','San Martin 215','Santiago','Metropolitana','PARTICULAR SUBVENCIONADO'),(2,2,'Santa Teresita',343434,'santateresita@gmail.com','Av. Ricardo Vicuña 236','Los Ángeles','Octaba','PARTICULAR SUBVENCIONADO'),(3,3,'LAVD',353535,'lavd@gmail.com','Linch 405','Los Ángeles','Octaba','PARTICULAR SUBVENCIONADO');
/*!40000 ALTER TABLE `colegio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curso` (
  `idcurso` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cf_idcolegio` int(11) NOT NULL,
  `año` year(4) DEFAULT NULL,
  `nivel` varchar(3) DEFAULT NULL,
  `letra` char(1) DEFAULT NULL,
  PRIMARY KEY (`idcurso`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
INSERT INTO `curso` VALUES (1,1,2020,'3EM','A'),(2,1,2021,'4EM','A'),(3,2,2020,'7EB','B'),(4,3,2020,'5EB','C');
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encargado`
--

DROP TABLE IF EXISTS `encargado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `encargado` (
  `idencargado` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cargo` varchar(50) DEFAULT NULL,
  `clave` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`idencargado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encargado`
--

LOCK TABLES `encargado` WRITE;
/*!40000 ALTER TABLE `encargado` DISABLE KEYS */;
INSERT INTO `encargado` VALUES (1,'JUAN BUSTOS',343434,'juanito@gmail.com','UTP','juan'),(2,'PEDRO GOMEZ',323232,'pedro@gmail.com','UTP','pedro'),(3,'PABLO BUSTOS',353535,'pablo@gmail.com','UTP','pablo');
/*!40000 ALTER TABLE `encargado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluacion`
--

DROP TABLE IF EXISTS `evaluacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evaluacion` (
  `idevaluacion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) DEFAULT NULL,
  `exigencia` float DEFAULT NULL,
  `asignatura` varchar(30) DEFAULT NULL,
  `puntaje_ideal` int(11) DEFAULT NULL,
  PRIMARY KEY (`idevaluacion`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluacion`
--

LOCK TABLES `evaluacion` WRITE;
/*!40000 ALTER TABLE `evaluacion` DISABLE KEYS */;
INSERT INTO `evaluacion` VALUES (1,'DOA Inicial',60,'MATEMATICA',39),(2,'DOA intermedio',60,'MATEMATICA',39),(3,'DOA final',60,'MATEMATICA',39),(4,'DOA inicial',60,'LENGUAJE',39),(5,'DOA intermedio',60,'LENGUAJE',39),(6,'DOA final',60,'LENGUAJE',39);
/*!40000 ALTER TABLE `evaluacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grafico`
--

DROP TABLE IF EXISTS `grafico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `grafico` (
  `idgrafico` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) DEFAULT NULL,
  `comentario` varchar(500) DEFAULT NULL,
  `cf_idcurso` int(11) NOT NULL,
  `cf_idevaluacion` int(11) NOT NULL,
  PRIMARY KEY (`idgrafico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grafico`
--

LOCK TABLES `grafico` WRITE;
/*!40000 ALTER TABLE `grafico` DISABLE KEYS */;
INSERT INTO `grafico` VALUES (1,'Distribución Nivel de Logro General','Se puede observar una distribución uniforme en las tres categorías de nivel de logro alcanzadas por los estudiantes.',1,1),(2,'Distribución de Calificaciones','Se puede observar una distribución uniforme en las tres categorías de nivel de logro alcanzadas por los estudiantes.',1,1),(3,'Comparativo','Se puede observar una distribución uniforme en las tres categorías de nivel de logro alcanzadas por los estudiantes.',1,1);
/*!40000 ALTER TABLE `grafico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pregunta`
--

DROP TABLE IF EXISTS `pregunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pregunta` (
  `idpregunta` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cf_idevaluacion` int(11) NOT NULL,
  `numero_pregunta` int(11) DEFAULT NULL,
  `contenido` varchar(100) DEFAULT NULL,
  `tipo` varchar(20) DEFAULT NULL,
  `puntaje` float DEFAULT NULL,
  `oa` varchar(300) DEFAULT NULL,
  `habilidad` varchar(20) DEFAULT NULL,
  `a` varchar(30) DEFAULT NULL,
  `b` varchar(30) DEFAULT NULL,
  `c` varchar(30) DEFAULT NULL,
  `d` varchar(30) DEFAULT NULL,
  `e` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idpregunta`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pregunta`
--

LOCK TABLES `pregunta` WRITE;
/*!40000 ALTER TABLE `pregunta` DISABLE KEYS */;
INSERT INTO `pregunta` VALUES (1,1,1,'1.2','ALTERNATIVA',1,NULL,'COMPRENSIÓN','DISTRACTOR 1','DISTRACTOR 2','CORRECTA','DISTRACTOR 3','DISTRACTOR 4'),(2,1,2,'1.2','ALTERNATIVA',1,NULL,'APLICACIÓN','DISTRACTOR 3','CORRECTA','DISTRACTOR 2','DISTRACTOR 1','DISTRACTOR 4'),(3,1,3,'3.2','ALTERNATIVA',1,NULL,'APLICACIÓN','CORRECTA','DISTRACTOR 3','DISTRACTOR 2','DISTRACTOR 1','DISTRACTOR 4'),(4,1,4,'3.2','ALTERNATIVA',1,NULL,'APLICACIÓN','DISTRACTOR 2','CORRECTA','DISTRACTOR 4','DISTRACTOR 1','DISTRACTOR 3'),(5,1,5,'3.2','ALTERNATIVA',1,NULL,'APLICACIÓN','DISTRACTOR 2','DISTRACTOR 1','CORRECTA','DISTRACTOR 4','DISTRACTOR 3'),(6,1,6,'5.1','ALTERNATIVA',1,NULL,'APLICACIÓN','DISTRACTOR 2','DISTRACTOR 4','DISTRACTOR 1','CORRECTA','DISTRACTOR 3'),(7,1,7,'5.1','ALTERNATIVA',1,NULL,'ANÁLISIS','DISTRACTOR 4','DISTRACTOR 2','CORRECTA','DISTRACTOR 1','DISTRACTOR 3'),(8,1,8,'5.1','ALTERNATIVA',1,NULL,'APLICACIÓN','DISTRACTOR 2','DISTRACTOR 1','DISTRACTOR 4','DISTRACTOR 3','CORRECTA'),(9,1,9,'5.1','ALTERNATIVA',1,NULL,'APLICACIÓN','DISTRACTOR 2','DISTRACTOR 1','DISTRACTOR 3','CORRECTA','DISTRACTOR 4'),(10,1,10,'5.1','ALTERNATIVA',1,NULL,'APLICACIÓN','DISTRACTOR 2','DISTRACTOR 1','CORRECTA','DISTRACTOR 3','DISTRACTOR 4'),(11,1,11,'6.1','ALTERNATIVA',1,NULL,'APLICACIÓN','DISTRACTOR 2','CORRECTA','DISTRACTOR 1','DISTRACTOR 3','DISTRACTOR 4'),(12,1,12,'6.1','ALTERNATIVA',1,NULL,'COMPRENSIÓN','DISTRACTOR 2','DISTRACTOR 1','CORRECTA','DISTRACTOR 3','DISTRACTOR 4'),(13,1,13,'6.1','ALTERNATIVA',1,NULL,'COMPRENSIÓN','CORRECTA','DISTRACTOR 1','DISTRACTOR 2','DISTRACTOR 3','DISTRACTOR 4'),(14,1,14,'6.1','ALTERNATIVA',1,NULL,'ANÁLISIS','DISTRACTOR 1','CORRECTA','DISTRACTOR 2','DISTRACTOR 3','DISTRACTOR 4'),(15,1,15,'6.1','ALTERNATIVA',1,NULL,'COMPRENSIÓN','DISTRACTOR 1','DISTRACTOR 3','CORRECTA','DISTRACTOR 2','DISTRACTOR 4'),(16,1,16,'6.1','ALTERNATIVA',1,NULL,'APLICACIÓN','DISTRACTOR 1','DISTRACTOR 3','DISTRACTOR 2','CORRECTA','DISTRACTOR 4'),(17,1,17,'6.1','ALTERNATIVA',1,NULL,'COMPRENSIÓN','DISTRACTOR 1','DISTRACTOR 3','CORRECTA','DISTRACTOR 2','DISTRACTOR 4'),(18,1,18,'13.1','ALTERNATIVA',1,NULL,'COMPRENSIÓN','DISTRACTOR 1','CORRECTA','DISTRACTOR 2','DISTRACTOR 3','DISTRACTOR 4'),(19,1,19,'13.1','ALTERNATIVA',1,NULL,'ANÁLISIS','DISTRACTOR 1','DISTRACTOR 3','CORRECTA','DISTRACTOR 2','DISTRACTOR 4'),(20,1,20,'13.1','ALTERNATIVA',1,NULL,'ANÁLISIS','DISTRACTOR 1','DISTRACTOR 3','DISTRACTOR 4','DISTRACTOR 2','CORRECTA'),(21,1,21,'13.1','ALTERNATIVA',1,NULL,'APLICACIÓN','DISTRACTOR 1','DISTRACTOR 3','DISTRACTOR 4','DISTRACTOR 2','CORRECTA'),(22,1,22,'17.1','ALTERNATIVA',1,NULL,'COMPRENSIÓN','DISTRACTOR 1','DISTRACTOR 3','DISTRACTOR 2','CORRECTA','DISTRACTOR 4'),(23,1,23,'17.1','ALTERNATIVA',1,NULL,'APLICACIÓN','CORRECTA','DISTRACTOR 1','DISTRACTOR 2','DISTRACTOR 4','DISTRACTOR 3'),(24,1,24,'22.1','ALTERNATIVA',1,NULL,'APLICACIÓN','CORRECTA','DISTRACTOR 1','DISTRACTOR 2','DISTRACTOR 4','DISTRACTOR 3'),(25,1,25,'22.1','ALTERNATIVA',1,NULL,'COMPRENSIÓN','DISTRACTOR 4','CORRECTA','DISTRACTOR 1','DISTRACTOR 2','DISTRACTOR 3'),(26,1,26,'27.1','DESARROLLO',3,NULL,'ANÁLISIS','DISTRACTOR 4','DISTRACTOR 3','CORRECTA','DISTRACTOR 1','DISTRACTOR 2'),(27,1,27,'27.1','DESARROLLO',2,NULL,'COMPRENSIÓN','DISTRACTOR 2','DISTRACTOR 3','DISTRACTOR 4','DISTRACTOR 1','CORRECTA');
/*!40000 ALTER TABLE `pregunta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `respuesta`
--

DROP TABLE IF EXISTS `respuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `respuesta` (
  `cf_idalumno` int(11) NOT NULL,
  `cf_idpregunta` int(11) NOT NULL,
  `respuesta` enum('A','B','C','D','E') DEFAULT NULL,
  PRIMARY KEY (`cf_idalumno`,`cf_idpregunta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `respuesta`
--

LOCK TABLES `respuesta` WRITE;
/*!40000 ALTER TABLE `respuesta` DISABLE KEYS */;
INSERT INTO `respuesta` VALUES (1,1,'A'),(1,2,'B'),(1,3,'A'),(1,4,'B'),(1,5,'C'),(1,6,'D'),(1,7,'E'),(1,8,'E'),(1,9,'D'),(1,10,'A'),(1,11,'B'),(1,12,'C'),(1,13,'D'),(1,14,'B'),(1,15,'C'),(1,16,'D'),(1,17,'E'),(1,18,'B'),(1,19,'C'),(1,20,'D'),(1,21,'E'),(1,22,'D'),(1,23,'A'),(1,24,'A'),(1,25,'B'),(1,26,'C'),(1,27,'A');
/*!40000 ALTER TABLE `respuesta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resultado`
--

DROP TABLE IF EXISTS `resultado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resultado` (
  `cf_idalumno` int(11) NOT NULL,
  `cf_idevaluacion` int(11) NOT NULL,
  `puntaje_total` float DEFAULT NULL,
  `nota` float DEFAULT NULL,
  `ptje_simce` int(11) DEFAULT NULL,
  `ptje_ptu` int(11) DEFAULT NULL,
  `porcentaje_logro` float DEFAULT NULL,
  `desempeño` enum('ALTO','MEDIO','MEDIO BAJO','INSUFICIENTE') DEFAULT NULL,
  PRIMARY KEY (`cf_idalumno`,`cf_idevaluacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resultado`
--

LOCK TABLES `resultado` WRITE;
/*!40000 ALTER TABLE `resultado` DISABLE KEYS */;
INSERT INTO `resultado` VALUES (1,1,7,2.8,216,423,37,'INSUFICIENTE'),(1,2,7,2.8,216,423,37,'INSUFICIENTE'),(1,3,7,2.8,216,423,37,'INSUFICIENTE'),(2,1,6,2.6,202,391,32,'INSUFICIENTE'),(2,2,6,2.6,202,391,32,'INSUFICIENTE'),(2,3,6,2.6,202,391,32,'INSUFICIENTE'),(3,1,4,2.1,175,307,21,'INSUFICIENTE'),(3,2,4,2.1,175,307,21,'INSUFICIENTE'),(3,3,4,2.1,175,307,21,'INSUFICIENTE'),(4,1,7,2.8,216,423,37,'INSUFICIENTE'),(4,2,7,2.8,216,423,37,'INSUFICIENTE'),(4,3,7,2.8,216,423,37,'INSUFICIENTE'),(5,1,5,2.3,188,353,26,'INSUFICIENTE'),(5,2,5,2.3,188,353,26,'INSUFICIENTE'),(5,3,5,2.3,188,353,26,'INSUFICIENTE'),(6,1,6,2.6,202,391,32,'INSUFICIENTE'),(6,2,6,2.6,202,391,32,'INSUFICIENTE'),(6,3,6,2.6,202,391,32,'INSUFICIENTE'),(7,1,4,2.1,275,307,21,'INSUFICIENTE'),(7,2,4,2.1,275,307,21,'INSUFICIENTE'),(7,3,4,2.1,275,307,21,'INSUFICIENTE'),(8,1,6,2.6,202,391,32,'INSUFICIENTE'),(8,2,6,2.6,202,391,32,'INSUFICIENTE'),(8,3,6,2.6,202,391,32,'INSUFICIENTE'),(9,1,7,2.8,216,423,37,'INSUFICIENTE'),(9,2,7,2.8,216,423,37,'INSUFICIENTE'),(9,3,7,2.8,216,423,37,'INSUFICIENTE'),(10,1,6,2.6,202,391,32,'INSUFICIENTE'),(10,2,6,2.6,202,391,32,'INSUFICIENTE'),(10,3,6,2.6,202,391,32,'INSUFICIENTE'),(11,1,7,2.8,216,423,37,'INSUFICIENTE'),(11,2,7,2.8,216,423,37,'INSUFICIENTE'),(11,3,7,2.8,216,423,37,'INSUFICIENTE'),(12,1,7,2.8,216,423,37,'INSUFICIENTE'),(12,2,7,2.8,216,423,37,'INSUFICIENTE'),(12,3,7,2.8,216,423,37,'INSUFICIENTE'),(13,1,6,2.6,202,391,32,'INSUFICIENTE'),(13,2,6,2.6,202,391,32,'INSUFICIENTE'),(13,3,6,2.6,202,391,32,'INSUFICIENTE'),(14,1,7,2.8,216,423,37,'INSUFICIENTE'),(14,2,7,2.8,216,423,37,'INSUFICIENTE'),(14,3,7,2.8,216,423,37,'INSUFICIENTE'),(15,1,6,2.6,202,391,32,'INSUFICIENTE'),(15,2,6,2.6,202,391,32,'INSUFICIENTE'),(15,3,6,5.5,202,650,32,'MEDIO'),(16,1,3,1.8,161,258,16,'INSUFICIENTE'),(16,2,3,1.8,161,258,16,'INSUFICIENTE'),(16,3,3,5.5,202,650,32,'MEDIO'),(17,1,6,2.6,202,391,32,'INSUFICIENTE'),(17,2,6,2.6,202,391,32,'INSUFICIENTE'),(17,3,6,5.5,202,650,32,'MEDIO'),(18,1,5,2.3,188,353,26,'INSUFICIENTE'),(18,2,5,2.3,188,353,26,'INSUFICIENTE'),(18,3,5,4.3,202,391,32,'MEDIO BAJO'),(19,1,6,2.6,202,391,32,'INSUFICIENTE'),(19,2,6,2.6,202,391,32,'INSUFICIENTE'),(19,3,6,4.3,202,391,32,'MEDIO BAJO'),(20,1,6,2.6,202,391,32,'INSUFICIENTE'),(20,2,6,2.6,202,391,32,'INSUFICIENTE'),(20,3,6,4.3,202,391,32,'MEDIO BAJO'),(21,1,6,2.6,202,391,32,'INSUFICIENTE'),(21,2,39,7,216,423,37,'ALTO'),(21,3,39,7,216,423,37,'ALTO'),(22,1,5,2.3,188,353,26,'INSUFICIENTE'),(22,2,39,7,216,423,37,'ALTO'),(22,3,39,7,216,423,37,'ALTO'),(23,1,7,2.8,216,423,37,'INSUFICIENTE'),(23,2,39,7,216,423,37,'ALTO'),(23,3,39,7,216,423,37,'ALTO'),(24,1,24,4.1,202,391,32,'MEDIO BAJO'),(24,2,24,4.8,202,391,32,'MEDIO BAJO'),(24,3,24,4.8,202,391,32,'MEDIO BAJO'),(25,1,24,4.1,202,391,32,'MEDIO BAJO'),(25,2,24,4.9,202,391,32,'MEDIO BAJO'),(25,3,24,4.9,202,391,32,'MEDIO BAJO'),(26,1,29,5.1,202,391,32,'MEDIO'),(26,2,29,5.1,202,650,32,'MEDIO'),(26,3,29,5.1,202,650,32,'MEDIO'),(27,1,29,5.1,202,391,32,'MEDIO'),(27,2,29,5.1,202,675,32,'MEDIO'),(27,3,29,5.1,202,675,32,'MEDIO'),(28,1,29,5.1,202,391,32,'MEDIO'),(28,2,29,6.1,202,688,32,'MEDIO'),(28,3,29,6.1,202,688,32,'MEDIO'),(29,1,29,5.1,202,391,32,'MEDIO'),(29,2,29,6.5,202,688,32,'MEDIO'),(29,3,29,6.5,202,688,32,'MEDIO'),(30,1,29,5.1,202,391,32,'ALTO'),(30,2,29,7,202,780,32,'ALTO'),(30,3,29,7,202,780,32,'ALTO'),(31,4,7,2.8,216,423,37,'INSUFICIENTE'),(32,4,6,2.6,202,391,32,'INSUFICIENTE'),(33,4,4,2.1,175,307,21,'INSUFICIENTE'),(34,4,7,2.8,216,423,37,'INSUFICIENTE'),(35,4,5,2.3,188,353,26,'INSUFICIENTE'),(36,4,6,2.6,202,391,32,'INSUFICIENTE'),(37,4,4,2.1,275,307,21,'INSUFICIENTE'),(38,4,6,2.6,202,391,32,'INSUFICIENTE'),(39,4,7,2.8,216,423,37,'INSUFICIENTE'),(40,4,6,2.6,202,391,32,'INSUFICIENTE'),(41,4,7,2.8,216,423,37,'INSUFICIENTE'),(42,4,7,2.8,216,423,37,'INSUFICIENTE'),(43,4,6,2.6,202,391,32,'INSUFICIENTE'),(44,4,7,2.8,216,423,37,'INSUFICIENTE'),(45,4,6,5.5,202,650,32,'MEDIO'),(46,4,3,5.5,202,650,32,'MEDIO'),(47,4,6,5.5,202,650,32,'MEDIO'),(48,4,5,4.3,202,391,32,'MEDIO BAJO'),(49,4,6,4.3,202,391,32,'MEDIO BAJO'),(50,4,6,4.3,202,391,32,'MEDIO BAJO'),(51,4,39,7,216,423,37,'ALTO'),(52,4,39,7,216,423,37,'ALTO'),(53,4,39,7,216,423,37,'ALTO'),(54,4,24,4.8,202,391,32,'MEDIO BAJO'),(55,4,24,4.9,202,391,32,'MEDIO BAJO');
/*!40000 ALTER TABLE `resultado` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-25 12:56:44
