<?php
    include "config.php";

    // Check user login or not
    if(!isset($_SESSION['uname'])){
        header('Location: index.php');
    }

    // logout
    if(isset($_POST['but_logout'])){
        session_destroy();
        header('Location: index.php');
    }
?>

<!DOCTYPE html> <!-- Para renderizar a los estándares actuales-->
<html lang="en">
<head>  
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
       
    <!-- Referencias Bootstrap -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">

    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
    
    <title>Resultados COA</title>
</head>
    
<body>

<header>
    <nav class="barra navbar navbar-expand-md navbar-dark">
        <a class="barra navbar-brand" href="home.php">Diagnóstico de Objetivos de Aprendizaje</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active"><a href="home.php" class="nav-link">Home</a></li>
                <li class="nav-item"><a href="./contacto.html" class="nav-link">Contacto</a></li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <form method="post" action="">
                        <input class="btn-cerrar nav-link navbar-dark bg-dark" type="submit" value="Cerrar Sesión" name="but_logout">
                    </form>
                </li>
            </ul>            
        </div>
    </nav>
</header>

<section class="container contenedor">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item" aria-current="page">Home</li>
            <li class="breadcrumb-item active" aria-current="page">Resultados Examen Inicial</li>
        </ol>
    </nav>
    
    <!-- Filtro -->
    <div class="form-row contenedor">
        <div class="form-group col-md-3">
            <label for="inputState">Habilidades</label>
            <select id="Opcion" name="Opcion" onchange="Opciones(event);"  class="form-control">
                <?php require_once "conexion.php"; 
                
                    $email=$_SESSION['uname'];                                                      
                    $sql="SELECT pregunta.habilidad
                    FROM pregunta JOIN evaluacion ON (pregunta.cf_idevaluacion = evaluacion.idevaluacion)
                    JOIN aplica ON (evaluacion.idevaluacion = aplica.cf_idevaluacion)
                    JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
                    JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
                    JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
                    WHERE encargado.email = '$email'
                    GROUP BY pregunta.habilidad";
                                                                                                                    
                    $result=mysqli_query($mysqli, $sql);

                    while( $row = mysqli_fetch_array($result, MYSQLI_ASSOC))
                    {
                        $habilidad=$row['habilidad'];
                ?>

                <option value="<?php echo $habilidad ?>"><?php echo $habilidad ?></option>

                <?php
                    }
                ?>

                <input type="hidden" id="habilidad" name="habilidad">

            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="inputState">Contenido</label>
            <select class="form-control">
                <?php require_once "conexion.php"; 
                                                                            
                    $sql="SELECT pregunta.oa
                    FROM pregunta JOIN evaluacion ON (pregunta.cf_idevaluacion = evaluacion.idevaluacion)
                    JOIN aplica ON (evaluacion.idevaluacion = aplica.cf_idevaluacion)
                    JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
                    JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
                    JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
                    WHERE encargado.email = '$email'
                    GROUP BY pregunta.oa";
                                                                                                                    
                    $result=mysqli_query($mysqli, $sql);

                    while( $row = mysqli_fetch_array($result, MYSQLI_ASSOC))
                    {
                        $oa=$row['oa'];
                ?>
                <option value="FiltroOA"><?php echo $oa ?></option>
                <?php
                    }
                ?>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="inputState">Asignatura</label>
            <select class="form-control">
                <?php require_once "conexion.php"; 
                                                                            
                    $sql="SELECT evaluacion.asignatura
                    FROM evaluacion JOIN aplica ON (evaluacion.idevaluacion = aplica.cf_idevaluacion)
                    JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
                    JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
                    JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
                    WHERE encargado.email = '$email'
                    GROUP BY evaluacion.asignatura";
                                                                                                                    
                    $result=mysqli_query($mysqli, $sql);

                    while( $row = mysqli_fetch_array($result, MYSQLI_ASSOC))
                    {
                        $asignatura=$row['asignatura'];
                ?>
                <option value="FiltroAsignatura"><?php echo $asignatura ?></option>
                <?php
                    }
                ?>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="inputState">Curso</label>
            <select class="form-control">
                <?php require_once "conexion.php"; 
                                                                            
                    $sql="SELECT curso.nivel, curso.letra, curso.año
                    FROM curso LEFT JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
                    JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
                    WHERE encargado.email = '$email'
                    GROUP BY curso.nivel, curso.letra, curso.año";
                                                                                                                    
                    $result=mysqli_query($mysqli, $sql);

                    while( $row = mysqli_fetch_array($result, MYSQLI_ASSOC))
                    {
                        $nivel=$row['nivel'];
                        $letra=$row['letra'];
                        $año=$row['año'];
                ?>
                <option value="FiltroCurso"><?php echo $nivel ?><?php echo $letra ?> - <?php echo $año ?></option>
                <?php
                    }
                ?>
            </select>
        </div> 
    </div>

    <!--Accordion wrapper-->
    <div class="accordion contenedor" id="accordionExample275">
    <!-- INFORME GENERAL -->
    <div class="card z-depth-0 bordered">
        <div class="acordeon-seleccion card-header" id="headingOne2">
        <h5 class="mb-0">
            <button class="acordeon btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne2"
            aria-expanded="true" aria-controls="collapseOne2">
            Informe General
            </button>
        </h5>
        </div>
        <div id="collapseOne2" class="collapse" aria-labelledby="headingOne2"
        data-parent="#accordionExample275">
        <div class="card-body">
            
            <?php require_once "conexion.php"; 
                
            $sql="SELECT evaluacion.nombre, aplica.fecha, aplica.promedio, curso.nivel, curso.letra, 
            evaluacion.asignatura, evaluacion.exigencia, evaluacion.puntaje_ideal
            FROM evaluacion JOIN aplica ON (evaluacion.idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            AND evaluacion.nombre LIKE '%Inicial'";
                                                            
            $result=mysqli_query($mysqli, $sql);
                                    
            while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $nombre=$row['nombre'];
                $fecha=$row['fecha'];
                $nivel=$row['nivel'];
                $letra=$row['letra'];
                $asignatura=$row['asignatura'];
                $exigencia=$row['exigencia'];
                $puntaje_ideal=$row['puntaje_ideal'];
            }
            ?>
                
            <div class="row">
                <table class="tabla col margen-derecha" >
                    <tr><th>Nombre Evaluación</th><td><? echo $nombre; ?></td></tr>
                    <tr><th>Fecha Evaluación</th><td><? echo $fecha; ?></td></tr>
                    <tr><th>Curso</th><td><? echo $nivel; ?><? echo $letra; ?></td></tr>
                    <tr><th>Asignatura</th><td><? echo $asignatura; ?></td></tr>
                    <tr><th>Exigencia</th><td><? echo $exigencia; ?>%</td></tr>
                    <tr><th>Puntaje Ideal</th><td><? echo $puntaje_ideal; ?> pts.</td></tr>
                </table>
                                        
                <?php require_once "conexion.php"; 
                    
                $sql="SELECT COUNT(resultado.cf_idalumno) AS rinde, aplica.promedio, aplica.desviacion_estandar, 
                aplica.porcentaje_insuficientes, aplica.nota_max, aplica.nota_min, aplica.ptje_simce, aplica.ptje_ptu
                FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
                JOIN aplica ON (evaluacion.idevaluacion = aplica.cf_idevaluacion)
                LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
                JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
                JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
                WHERE encargado.email='$email'
                AND evaluacion.nombre LIKE '%Inicial'
                GROUP BY aplica.promedio, aplica.desviacion_estandar, aplica.porcentaje_insuficientes, 
                aplica.nota_max, aplica.nota_min, aplica.ptje_simce, aplica.ptje_ptu";
                                                                    
                $result=mysqli_query($mysqli, $sql);
                                    
                while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    $rinde=$row['rinde'];
                    $promedio=$row['promedio'];
                    $desviacion_estandar=$row['desviacion_estandar'];
                    $porcentaje_insuficientes=$row['porcentaje_insuficientes'];
                    $nota_max=$row['nota_max'];
                    $nota_min=$row['nota_min'];
                    $ptje_simce=$row['ptje_simce'];
                    $ptje_ptu=$row['ptje_ptu'];
                }
                ?>
                                        
                <table class="tabla col marge-izquierda" >
                    <tr><th>Total Rinden</th><td><? echo $rinde; ?></td></tr>
                    <tr><th>Promedio</th><td><? echo $promedio; ?></td></tr>
                    <tr><th>Desv. Estándar</th><td><? echo $desviacion_estandar; ?></td></tr>
                    <tr><th>% Insuficiente</th><td><? echo $porcentaje_insuficientes; ?>%</td></tr>
                    <tr><th>Nota más alta</th><td><? echo $nota_max; ?></td></tr>
                    <tr><th>Nota más baja</th><td><? echo $nota_min; ?></td></tr>
                </table>
                                                
            </div>
                
            <div class="row">
                <table class="tabla margen-arriba">
                    <tr><th>Pje. SIMCE aprox.</th><td><? echo $ptje_simce; ?></td></tr>
                    <tr><th>Pje. PTU aprox.</th><td><? echo $ptje_ptu; ?></td></tr>
                </table>
            </div>
        </div>
        </div>
    </div>

    <!-- NIVEL DE LOGRO GENERAL -->
    <div class="card z-depth-0 bordered">
        <div class="acordeon-seleccion card-header" id="headingTwo2">
        <h5 class="mb-0">
            <button class="acordeon btn btn-link collapsed" type="button" data-toggle="collapse"
            data-target="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
            Nivel de Logro General
            </button>
        </h5>
        </div>
        <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo2"
        data-parent="#accordionExample275">
        <div class="card-body">
            
            <?php require_once "conexion.php"; 
                            
            $sql="SELECT ROUND(((SELECT COUNT(desempeño) FROM resultado WHERE desempeño='INSUFICIENTE')*100/(SELECT COUNT(desempeño) FROM resultado)),1) AS insuficiente, 
            ROUND(((SELECT COUNT(desempeño) FROM resultado WHERE desempeño='MEDIO BAJO')*100/(SELECT COUNT(desempeño) FROM resultado)),1) AS medio_bajo, 
            ROUND(((SELECT COUNT(desempeño) FROM resultado WHERE desempeño='MEDIO')*100/(SELECT COUNT(desempeño) FROM resultado)),1) AS medio,
            ROUND(((SELECT COUNT(desempeño) FROM resultado WHERE desempeño='ALTO')*100/(SELECT COUNT(desempeño) FROM resultado)),1) AS alto,
            grafico.comentario
            FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (evaluacion.idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN grafico ON (grafico.cf_idcurso = curso.idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            AND evaluacion.nombre LIKE '%Inicial'
            GROUP BY insuficiente, medio_bajo, medio, insuficiente, comentario";
                                            
            $result=mysqli_query($mysqli, $sql);
                        
            while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $insuficiente=$row['insuficiente'];
                $medio_bajo=$row['medio_bajo'];
                $medio=$row['medio'];
                $alto=$row['alto'];
                $comentario=$row['comentario'];
            }
                            
            ?>

            <canvas id="myChart" width="200" height="80"></canvas>
            <script type="text/javascript">

                var ctx = document.getElementById('myChart').getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ['Insuficiente', 'Medio Bajo', 'Medio Alto', 'Alto'],
                        datasets: [{
                            label: '#',
                            data: [<?php echo $insuficiente; ?>, <?php echo $medio_bajo; ?>, <?php echo $medio; ?>, <?php echo $alto; ?>],
                            backgroundColor: [
                                'rgba(255, 0, 0, 0.2)',
                                'rgba(247, 172, 72, 0.2)',
                                'rgba(255, 255, 0, 0.2)',
                                'rgba(0, 255, 0, 0.2)',                            
                            ],
                            borderColor: [
                                'rgba(49,88,119,1)',
                                'rgba(49,88,119,1)',
                                'rgba(49,88,119,1)',
                                'rgba(49,88,119,1)'
                            ],
                            borderWidth: 1
                            }]
                        },
                    options: {
                        cutoutPercentage: 0,
                        title: {
                            display: true,
                            text: 'DISTRIBUCIÓN NIVEL DE LOGRO GENERAL',
                            position: 'top',
                            fontSize: 15
                        }
                    }
                });
            </script>
                            
            </div >
                <h4 style="margin-left:20px;">Comentario: </h4>
                <p style="margin-left:20px;"><?php echo $comentario; ?></p>
            </div>
        </div>
        </div>
    </div>

    <!-- DISTRIBUCION DE CALIFICACIONES -->
    <div class="card z-depth-0 bordered">
        <div class="acordeon-seleccion card-header" id="headingThree2">
        <h5 class="mb-0">
            <button class="acordeon btn btn-link collapsed" type="button" data-toggle="collapse"
            data-target="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2">
            Distribución de Calificaciones
            </button>
        </h5>
        </div>
        <div id="collapseThree2" class="collapse" aria-labelledby="headingThree2"
        data-parent="#accordionExample275">
        <div class="card-body">
            
            <?php require_once "conexion.php"; 
                            
            $sql="SELECT (SELECT COUNT(cf_idalumno) FROM resultado WHERE nota BETWEEN 0 AND 1.4) AS cantidad1,
            (SELECT COUNT(cf_idalumno) FROM resultado WHERE nota BETWEEN 1.5 AND 1.9) AS cantidad2,
            (SELECT COUNT(cf_idalumno) FROM resultado WHERE nota BETWEEN 2.0 AND 2.4) AS cantidad3,
            (SELECT COUNT(cf_idalumno) FROM resultado WHERE nota BETWEEN 2.5 AND 2.9) AS cantidad4,
            (SELECT COUNT(cf_idalumno) FROM resultado WHERE nota BETWEEN 3.0 AND 3.4) AS cantidad5,
            (SELECT COUNT(cf_idalumno) FROM resultado WHERE nota BETWEEN 3.5 AND 3.9) AS cantidad6,
            (SELECT COUNT(cf_idalumno) FROM resultado WHERE nota BETWEEN 4.0 AND 4.7) AS cantidad7,
            (SELECT COUNT(cf_idalumno) FROM resultado WHERE nota BETWEEN 4.8 AND 5.4) AS cantidad8,
            (SELECT COUNT(cf_idalumno) FROM resultado WHERE nota BETWEEN 5.5 AND 6.2) AS cantidad9,
            (SELECT COUNT(cf_idalumno) FROM resultado WHERE nota BETWEEN 6.3 AND 7.0) AS cantidad10,
            grafico.comentario
            FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (evaluacion.idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN grafico ON (grafico.cf_idcurso = curso.idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            AND evaluacion.nombre LIKE '%Inicial'
            GROUP BY cantidad1,cantidad2,cantidad3,cantidad4,cantidad5,cantidad6,cantidad7,cantidad8,cantidad9,cantidad10,comentario";
                                            
            $result=mysqli_query($mysqli, $sql);
                    
            while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $cantidad1=$row['cantidad1'];
                $cantidad2=$row['cantidad2'];
                $cantidad3=$row['cantidad3'];
                $cantidad4=$row['cantidad4'];
                $cantidad5=$row['cantidad5'];
                $cantidad6=$row['cantidad6'];
                $cantidad7=$row['cantidad7'];
                $cantidad8=$row['cantidad8'];
                $cantidad9=$row['cantidad9'];
                $cantidad10=$row['cantidad10'];
                $comentario=$row['comentario'];
            }
                        
            ?>

            <canvas id="myChart2" width="200" height="80"></canvas>
            <script>
                var dataValues = [<?php echo $cantidad1; ?>, <?php echo $cantidad2; ?>, <?php echo $cantidad3; ?>, <?php echo $cantidad4; ?>, <?php echo $cantidad5; ?>, <?php echo $cantidad6; ?>, <?php echo $cantidad7; ?>, <?php echo $cantidad8; ?>, <?php echo $cantidad9; ?>, <?php echo $cantidad10; ?>];
                var dataValues2 = [<?php echo $cantidad1; ?>, <?php echo $cantidad2; ?>, <?php echo $cantidad3; ?>, <?php echo $cantidad4; ?>, <?php echo $cantidad5; ?>, <?php echo $cantidad6; ?>, <?php echo $cantidad7; ?>, <?php echo $cantidad8; ?>, <?php echo $cantidad9; ?>, <?php echo $cantidad10; ?>];
                var ctx = document.getElementById('myChart2').getContext('2d');
                var myChart2 = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['[. , 1.5[', '[1.5 , 2.0[', '[2.0 , 2.5[', '[2.5 , 3.0[', '[3.0 , 3.5[', '[3.5 , 4.0[', '[4.0 , 4.8[', '[4.8 , 5.5[', '[5.5 , 6.3[', '[6.3 , 7.0]'],
                        datasets: [{
                            label: ['Grupo A'],
                            data: dataValues,
                            backgroundColor: [
                                'rgba(255, 0, 0, 0.5)',
                                'rgba(255, 0, 0, 0.5)',
                                'rgba(255, 0, 0, 0.5)',
                                'rgba(255, 0, 0, 0.5)',
                                'rgba(247, 172, 72, 0.5)',
                                'rgba(247, 172, 72, 0.5)',
                                'rgba(255, 255, 0, 0.5)',
                                'rgba(255, 255, 0, 0.5)',
                                'rgba(0, 255, 0, 0.5)',
                                'rgba(0, 255, 0, 0.5)',
                            ],
                            borderColor: [

                            ],
                            borderWidth: 1
                        },
                        {
                            label: 'Line Dataset',
                            data: dataValues2,
                            type: 'line',
                            // this dataset is drawn on top
                            order: 3,
                            backgroundColor: 'rgba(247, 172, 72, 0)',
                            borderColor: 'rgba(60, 197, 255, 1)',
                            borderWidth: 1,                                   
                        }]
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'DISTRIBUCIÓN DE CALIFICACIONES',
                            position: 'top',
                            fontSize:15
                        },
                        legend: {
                            display: false
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                            xAxes: [{
                                barPercentage: 1.245,
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                        },                        
                    } 
                });
            </script>

            <div class="comentarios">
                <h4>Comentario: </h4>
                <p><?php echo $comentario; ?></p>
            </div>
        </div>
        </div>
    </div>

    <!-- COMPARATIVO DE DESEMPEÑO POR GENERO -->
    <div class="card z-depth-0 bordered">
        <div class="acordeon-seleccion card-header" id="headingFour2">
        <h5 class="mb-0">
            <button class="acordeon btn btn-link collapsed" type="button" data-toggle="collapse"
            data-target="#collapseFour2" aria-expanded="false" aria-controls="collapseFour2">
            Comparativo de Desempeño por Genero
            </button>
        </h5>
        </div>
        <div id="collapseFour2" class="collapse" aria-labelledby="headingFour2"
        data-parent="#accordionExample275">
        <div class="card-body">

            <?php require_once "conexion.php"; 

            $sql="SELECT (SELECT COUNT(resultado.desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
            WHERE desempeño='INSUFICIENTE' AND genero = 'MASCULINO' AND evaluacion.nombre LIKE '%Inicial') 
            AS insuficienteM, 
            (SELECT COUNT(resultado.desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE desempeño='MEDIO BAJO' AND genero = 'MASCULINO' AND evaluacion.nombre LIKE '%Inicial') 
            AS medio_bajoM, 
            (SELECT COUNT(resultado.desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)  
            WHERE desempeño='MEDIO' AND genero = 'MASCULINO' AND evaluacion.nombre LIKE '%Inicial') 
            AS medioM,
            (SELECT COUNT(resultado.desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE desempeño='ALTO' AND genero = 'MASCULINO' AND evaluacion.nombre LIKE '%Inicial') 
            AS altoM,
            (SELECT COUNT(resultado.desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE desempeño='INSUFICIENTE' AND genero = 'FEMENINO' AND evaluacion.nombre LIKE '%Inicial') 
            AS insuficienteF, 
            (SELECT COUNT(resultado.desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE desempeño='MEDIO BAJO' AND genero = 'FEMENINO' AND evaluacion.nombre LIKE '%Inicial') 
            AS medio_bajoF, 
            (SELECT COUNT(resultado.desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE desempeño='MEDIO' AND genero = 'FEMENINO' AND evaluacion.nombre LIKE '%Inicial') 
            AS medioF,
            (SELECT COUNT(resultado.desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE desempeño='ALTO' AND genero = 'FEMENINO' AND evaluacion.nombre LIKE '%Inicial') 
            AS altoF,
            grafico.comentario
            FROM resultado JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN grafico ON (grafico.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (grafico.cf_idevaluacion = aplica.cf_idevaluacion)
            JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            GROUP BY insuficienteM, evaluacion.nombre, medio_bajoM, medioM, altoM, insuficienteF, medio_bajoF, medioF, altoF, comentario";
                                                        
            $result=mysqli_query($mysqli, $sql);
                                    
            while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $insuficienteM=$row['insuficienteM'];
            $medio_bajoM=$row['medio_bajoM'];
            $medioM=$row['medioM'];
            $altoM=$row['altoM'];
            $insuficienteF=$row['insuficienteF'];
            $medio_bajoF=$row['medio_bajoF'];
            $medioF=$row['medioF'];
            $altoF=$row['altoF'];
            $comentario=$row['comentario'];
            }
                                    
            ?>

            <canvas id="myChart3"></canvas>
            <script type="text/javascript">
            var ctx = document.getElementById('myChart3').getContext('2d');
            var myChart3 = new Chart(ctx, {
                type: 'bar',
                data: {
                labels: ['Insuficiente', 'Medio Bajo', 'Medio', 'Alto'],
                datasets: [{
                    label: 'Masculino',
                    data: [<?php echo $insuficienteM; ?>, <?php echo $medio_bajoM; ?>, <?php echo $medioM; ?>, <?php echo $altoM; ?>],
                    backgroundColor: [
                    'rgba(80, 204, 66, 0.4)',
                    'rgba(80, 204, 66, 0.4)',
                    'rgba(80, 204, 66, 0.4)',
                    'rgba(80, 204, 66, 0.4)'
                    ],
                    borderColor: [
                    'rgba(108, 157, 162, 1)',
                    'rgba(108, 157, 162, 1)',
                    'rgba(108, 157, 162, 1)',
                    'rgba(108, 157, 162, 1)'
                    ],
                    borderWidth: 1
                },
                {
                    label: 'Femenino',
                    data: [<?php echo $insuficienteF; ?>, <?php echo $medio_bajoF; ?>, <?php echo $medioF; ?>, <?php echo $altoF; ?>],
                    backgroundColor: [
                    'rgba(24, 61, 120, 0.4)',
                    'rgba(24, 61, 120, 0.4)',
                    'rgba(24, 61, 120, 0.4)',
                    'rgba(24, 61, 120, 0.4)'
                    ],
                    borderColor: [
                    'rgba(108, 157, 162, 1)',
                    'rgba(108, 157, 162, 1)',
                    'rgba(108, 157, 162, 1)',
                    'rgba(108, 157, 162, 1)'
                    ],
                    borderWidth: 1
                }
                ]},
                options: {
                cutoutPercentage: 0,
                title: {
                    display: true,
                    text: 'COMPARACIÓN DE RESULTADOS POR GÉNERO',
                    position: 'top',
                    fontSize: 15
                },
                    scales: {
                    yAxes: [{ticks: {beginAtZero: true,},}]
                    },
                }
            });
            </script>

            <?php require_once "conexion.php"; 

            $sql="SELECT femenino.min AS minF, femenino.max AS maxF, ROUND(femenino.promedio,0) AS promedioF, ROUND(femenino.varianza,0) AS varianzaF, ROUND(femenino.simce,0) AS simceF, ROUND(femenino.ptu,0) AS ptuF,
            masculino.min AS minM, masculino.max AS maxM, ROUND(masculino.promedio,0) AS promedioM, ROUND(masculino.varianza,0) AS varianzaM, ROUND(masculino.simce,0) AS simceM, ROUND(masculino.ptu,0) AS ptuM,
            grafico.comentario
            FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN grafico ON (grafico.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (grafico.cf_idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN (SELECT alumno.cf_idcurso, MIN(resultado.nota) AS min, MAX(resultado.nota) AS max, AVG(resultado.nota) AS promedio, 
            VARIANCE(resultado.nota) AS varianza, AVG(ptje_simce) AS simce, AVG(ptje_ptu) AS ptu
            FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion) 
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
            WHERE genero = 'FEMENINO'
            AND evaluacion.nombre LIKE '%Inicial'
            GROUP BY cf_idcurso) AS femenino
            ON (curso.idcurso = femenino.cf_idcurso)
            RIGHT JOIN alumno ON (femenino.cf_idcurso = alumno.cf_idcurso)
            JOIN (SELECT alumno.cf_idcurso, MIN(resultado.nota) AS min, MAX(resultado.nota) AS max, AVG(resultado.nota) AS promedio, 
            VARIANCE(resultado.nota) AS varianza, AVG(ptje_simce) AS simce, AVG(ptje_ptu) AS ptu
            FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
            WHERE genero = 'MASCULINO'
            AND evaluacion.nombre LIKE '%Inicial'
            GROUP BY cf_idcurso) AS masculino
            ON (alumno.cf_idcurso = masculino.cf_idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            GROUP BY femenino.min, femenino.max,femenino.promedio, femenino.simce, femenino.ptu, varianzaF, varianzaM,
            masculino.min, masculino.max,masculino.promedio, masculino.simce, masculino.ptu, grafico.comentario";
                                                
            $result=mysqli_query($mysqli, $sql);
                            
            while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $minF=$row['minF'];
            $maxF=$row['maxF'];
            $promedioF=$row['promedioF'];
            $varianzaF=$row['varianzaF'];
            $simceF=$row['simceF'];
            $ptuF=$row['ptuF'];
            $minM=$row['minM'];
            $maxM=$row['maxM'];
            $promedioM=$row['promedioM'];
            $varianzaM=$row['varianzaM'];
            $simceM=$row['simceM'];
            $ptuM=$row['ptuM'];
            $comentario=$row['comentario'];
            }
                            
            ?>

            <div class="row">
            <table class="tabla col margen-derecha">
                <tr><th colspan="2">FEMENINO</th></tr>
                <tr><td>Nota Mínima</td><td><?php echo $minF; ?></td></tr>
                <tr><td>Nota Máxima</td><td><?php echo $maxF; ?></td></tr>
                <tr><td>Promedio</td><td><?php echo $promedioF; ?></td></tr>
                <tr><td>Variación</td><td><?php echo $varianzaF; ?> %</td></tr>
                <tr><td>Prom. SIMCE aprox.</td><td><?php echo $simceF; ?></td></tr>
                <tr><td>Prom. PTU aprox.</td><td><?php echo $ptuF; ?></td></tr>
            </table>
                                            
            <table class="tabla col margen-izquierda ">
                <tr><th colspan="2">MASCULINO</th></tr>
                <tr><td>Nota Mínima</td><td><?php echo $minM; ?></td></tr>
                <tr><td>Nota Máxima</td><td><?php echo $maxM; ?></td></tr>
                <tr><td>Promedio</td><td><?php echo $promedioM; ?></td></tr>
                <tr><td>Variación</td><td><?php echo $varianzaM; ?> %</td></tr>
                <tr><td>Prom. SIMCE aprox.</td><td><?php echo $simceM; ?></td></tr>
                <tr><td>Prom. PTU aprox.</td><td><?php echo $ptuM; ?></td></tr>
            </table>   
            </div>

            <div class="comentarios">
            <h4>Comentario: </h4>
            <p><?php echo $comentario; ?></p>
            </div>
        </div>
        </div>
    </div>

    <!-- DESEMPEÑO POR ANTIGUEDAD -->
    <div class="card z-depth-0 bordered">
        <div class="acordeon-seleccion card-header" id="headingFive2">
        <h5 class="mb-0">
            <button class="acordeon btn btn-link collapsed" type="button" data-toggle="collapse"
            data-target="#collapseFive2" aria-expanded="false" aria-controls="collapseFive2">
            Desempeño por Antigüedad
            </button>
        </h5>
        </div>
        <div id="collapseFive2" class="collapse" aria-labelledby="headingFive2"
        data-parent="#accordionExample275">
        <div class="card-body">

            <?php require_once "conexion.php"; 

            $sql="SELECT (SELECT COUNT(resultado.desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE resultado.desempeño='INSUFICIENTE' AND ingreso = 'Antiguo' AND evaluacion.nombre LIKE '%Inicial') 
            AS insuficienteA, 
            (SELECT COUNT(desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE desempeño='MEDIO BAJO' AND ingreso = 'Antiguo' AND evaluacion.nombre LIKE '%Inicial') 
            AS medio_bajoA, 
            (SELECT COUNT(desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)  
            WHERE desempeño='MEDIO' AND ingreso = 'Antiguo' AND evaluacion.nombre LIKE '%Inicial') 
            AS medioA,
            (SELECT COUNT(desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)  
            WHERE desempeño='ALTO' AND ingreso = 'Antiguo' AND evaluacion.nombre LIKE '%Inicial') 
            AS altoA,
            (SELECT COUNT(desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE desempeño='INSUFICIENTE' AND ingreso = 'Nuevo' AND evaluacion.nombre LIKE '%Inicial') 
            AS insuficienteN, 
            (SELECT COUNT(desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE desempeño='MEDIO BAJO' AND ingreso = 'Nuevo' AND evaluacion.nombre LIKE '%Inicial') 
            AS medio_bajoN, 
            (SELECT COUNT(desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno) 
            WHERE desempeño='MEDIO' AND ingreso = 'Nuevo' AND evaluacion.nombre LIKE '%Inicial') 
            AS medioN,
            (SELECT COUNT(desempeño) FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)  
            WHERE desempeño='ALTO' AND ingreso = 'Nuevo' AND evaluacion.nombre LIKE '%Inicial') 
            AS altoN
            FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN grafico ON (grafico.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (grafico.cf_idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            GROUP BY insuficienteN, medio_bajoN, medioN, altoN, insuficienteA, medio_bajoA, medioA, altoA";

            $result=mysqli_query($mysqli, $sql);
                            
            while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $insuficienteA=$row['insuficienteA'];
            $medio_bajoA=$row['medio_bajoA'];
            $medioA=$row['medioA'];
            $altoA=$row['altoA'];
            $insuficienteN=$row['insuficienteN'];
            $medio_bajoN=$row['medio_bajoN'];
            $medioN=$row['medioN'];
            $altoN=$row['altoN'];
            }

            ?>

            <canvas id="myChart4" width="400" height="180"></canvas>
            <script>
            var ctx = document.getElementById('myChart4').getContext('2d');
            var myChart4 = new Chart(ctx, {
                type: 'bar',
                data: {
                labels: ['Insuficiente', 'Medio Bajo', 'Medio Alto', 'Adecuado'],
                datasets: [{
                    label: 'Antiguo',
                    data: [<?php echo $insuficienteA; ?>, <?php echo $medio_bajoA; ?>, <?php echo $medioA; ?>, <?php echo $altoA; ?>],
                    backgroundColor: [
                    'rgba(190, 190, 190, 0.3)',
                    'rgba(190, 190, 190, 0.3)',
                    'rgba(190, 190, 190, 0.3)',
                    'rgba(190, 190, 190, 0.3)'
                    ],
                    borderColor: [
                    'rgba(108, 157, 162, 1)',
                    'rgba(108, 157, 162, 1)',
                    'rgba(108, 157, 162, 1)',
                    'rgba(108, 157, 162, 1)'                        
                    ],
                    borderWidth: 1
                },
                {
                label: 'Nuevo Ingreso',
                data: [<?php echo $insuficienteN; ?>, <?php echo $medio_bajoN; ?>, <?php echo $medioN; ?>, <?php echo $altoN; ?>],
                backgroundColor: [
                    'rgba(61, 24, 120, 0.4)',
                    'rgba(61, 24, 120, 0.4)',
                    'rgba(61, 24, 120, 0.4)',
                    'rgba(61, 24, 120, 0.4)'
                ],
                borderColor: [
                    'rgba(157, 108, 162, 1)',
                    'rgba(157, 108, 162, 1)',
                    'rgba(157, 108, 162, 1)',
                    'rgba(157, 108, 162, 1)'                                                 
                ],
                borderWidth: 1
                }
                ]},
                options: {
                cutoutPercentage: 0,
                title: {
                    display: true,
                    text: 'COMPARACIÓN DE RESULTADOS POR ANTIGUEDAD',
                    position: 'top',
                    fontSize:15
                },
                scales: {
                    yAxes: [{ticks: {beginAtZero: true,},}]
                },
                }
            });
            </script>

            <?php require_once "conexion.php"; 
                    
            $sql="SELECT antiguo.min AS minA, antiguo.max AS maxA, ROUND(antiguo.promedio,0) AS promedioA, ROUND(antiguo.simce,0) AS simceA, ROUND(antiguo.ptu,0) AS ptuA,
            nuevo.min AS minN, nuevo.max AS maxN, ROUND(nuevo.promedio,0) AS promedioN, ROUND(nuevo.simce,0) AS simceN, ROUND(nuevo.ptu,0) AS ptuN,
            ROUND(nuevo.varianza,0) AS varianzaN, ROUND(antiguo.varianza,0) AS varianzaA, grafico.comentario
            FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN grafico ON (grafico.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (grafico.cf_idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN (SELECT alumno.cf_idcurso, MIN(resultado.nota) AS min, MAX(resultado.nota) AS max, AVG(resultado.nota) AS promedio, 
                VARIANCE(resultado.nota) AS varianza, AVG(ptje_simce) AS simce, AVG(ptje_ptu) AS ptu
                FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
                JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
                WHERE alumno.ingreso = 'Antiguo'
                AND evaluacion.nombre LIKE '%Inicial'
                GROUP BY cf_idcurso) AS antiguo
            ON (curso.idcurso = antiguo.cf_idcurso)
            RIGHT JOIN alumno ON (antiguo.cf_idcurso = alumno.cf_idcurso)
            JOIN (SELECT alumno.cf_idcurso, MIN(resultado.nota) AS min, MAX(resultado.nota) AS max, AVG(resultado.nota) AS promedio, 
                VARIANCE(resultado.nota) AS varianza, AVG(ptje_simce) AS simce, AVG(ptje_ptu) AS ptu
                FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
                JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
                WHERE alumno.ingreso = 'Nuevo'
                AND evaluacion.nombre LIKE '%Inicial'
                GROUP BY cf_idcurso) AS nuevo
            ON (alumno.cf_idcurso = nuevo.cf_idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='juanito@gmail.com'
            GROUP BY antiguo.min,antiguo.max,antiguo.promedio,antiguo.simce,antiguo.ptu,
            nuevo.min,nuevo.max,nuevo.promedio,nuevo.simce,nuevo.ptu,grafico.comentario,varianzaN,varianzaA";
                            
            $result=mysqli_query($mysqli, $sql);

            while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $minA=$row['minA'];
            $maxA=$row['maxA'];
            $promedioA=$row['promedioA'];
            $varianzaA=$row['varianzaA'];
            $simceA=$row['simceA'];
            $ptuA=$row['ptuA'];
            $minN=$row['minN'];
            $maxN=$row['maxN'];
            $promedioN=$row['promedioN'];
            $varianzaN=$row['varianzaN'];
            $simceN=$row['simceN'];
            $ptuN=$row['ptuN'];
            $comentario=$row['comentario'];
            }

            ?>

            <div class="row">
            <table class="tabla col margen-derecha">
                <tr><th colspan="2">ANTIGUO</th></tr>
                <tr><td>Nota Mínima</td><td><?php echo $minA; ?></td></tr>
                <tr><td>Nota Máxima</td><td><?php echo $minA; ?></td></tr>
                <tr><td>Promedio</td><td><?php echo $promedioA; ?></td></tr>
                <tr><td>Variación</td><td><?php echo $varianzaA; ?> %</td></tr>
                <tr><td>Prom. SIMCE aprox.</td><td><?php echo $simceA; ?></td></tr>
                <tr><td>Prom. PTU aprox.</td><td><?php echo $ptuA; ?></td></tr>
            </table>

            <table class="tabla col margen-izquierda">
                <tr><th colspan="2">NUEVO INGRESO</th></tr>
                <tr><td>Nota Mínima</td><td><?php echo $minN; ?></td></tr>
                <tr><td>Nota Máxima</td><td><?php echo $minN; ?></td></tr>
                <tr><td>Promedio</td><td><?php echo $promedioN; ?></td></tr>
                <tr><td>Variación</td><td><?php echo $varianzaN; ?> %</td></tr>
                <tr><td>Prom. SIMCE aprox.</td><td><?php echo $simceN; ?></td></tr>
                <tr><td>Prom. PTU aprox.</td><td><?php echo $ptuN; ?></td></tr>
            </table>
            </div>

            <div class="comentarios">
            <h4>Comentario: </h4>
            <p><?php echo $comentario; ?></p>
            </div>
        </div>
        </div>
    </div>

    <!-- RESUMEN DE DESEMPEÑO -->
    <div class="card z-depth-0 bordered">
        <div class="acordeon-seleccion card-header" id="headingSix2">
        <h5 class="mb-0">
            <button class="acordeon btn btn-link collapsed" type="button" data-toggle="collapse"
            data-target="#collapseSix2" aria-expanded="false" aria-controls="collapseSix2">
            Resumen de Desempeño
            </button>
        </h5>
        </div>
        <div id="collapseSix2" class="collapse" aria-labelledby="headingSix2"
        data-parent="#accordionExample275">
        <div class="card-body">
        
            <?php require_once "conexion.php"; 
                        
            $sql="SELECT insuficiente.cantidad AS cantidadI, ROUND(insuficiente.promedio,0) AS promedioI, ROUND(insuficiente.simce,0) AS simceI, ROUND(insuficiente.ptu,0) AS ptuI,
            medio_bajo.cantidad AS cantidadMB, ROUND(medio_bajo.promedio,0) AS promedioMB, ROUND(medio_bajo.simce,0) AS simceMB, ROUND(medio_bajo.ptu,0) AS ptuMB,
            medio.cantidad AS cantidadM, ROUND(medio.promedio,0) AS promedioM, ROUND(medio.simce,0) AS simceM, ROUND(medio.ptu,0) AS ptuM,
            alto.cantidad AS cantidadA, ROUND(alto.promedio,0) AS promedioA, ROUND(medio_bajo.simce,0) AS simceA, ROUND(medio_bajo.ptu,0) AS ptuA
            FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN grafico ON (grafico.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (grafico.cf_idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN (SELECT alumno.cf_idcurso, COUNT(alumno.cf_idcurso) AS cantidad, AVG(resultado.nota) AS promedio, 
            AVG(ptje_simce) AS simce, AVG(ptje_ptu) AS ptu
            FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
            WHERE resultado.desempeño = 'INSUFICIENTE'
            AND evaluacion.nombre LIKE '%Inicial'
            GROUP BY cf_idcurso) AS insuficiente
            ON (curso.idcurso = insuficiente.cf_idcurso)
            RIGHT JOIN alumno ON (insuficiente.cf_idcurso = alumno.cf_idcurso)
            JOIN (SELECT alumno.cf_idcurso, COUNT(alumno.cf_idcurso) AS cantidad, AVG(resultado.nota) AS promedio, 
            AVG(ptje_simce) AS simce, AVG(ptje_ptu) AS ptu
            FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
            WHERE resultado.desempeño = 'MEDIO BAJO'
            AND evaluacion.nombre LIKE '%Inicial'
            GROUP BY cf_idcurso) AS medio_bajo
            ON (alumno.cf_idcurso = medio_bajo.cf_idcurso)
            JOIN (SELECT alumno.cf_idcurso, COUNT(alumno.cf_idcurso) AS cantidad, AVG(resultado.nota) AS promedio, 
            AVG(ptje_simce) AS simce, AVG(ptje_ptu) AS ptu
            FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
            WHERE resultado.desempeño = 'MEDIO'
            AND evaluacion.nombre LIKE '%Inicial'
            GROUP BY cf_idcurso) AS medio
            ON (medio_bajo.cf_idcurso = medio.cf_idcurso)
            JOIN (SELECT alumno.cf_idcurso, COUNT(alumno.cf_idcurso) AS cantidad, AVG(resultado.nota) AS promedio, 
            AVG(ptje_simce) AS simce, AVG(ptje_ptu) AS ptu
            FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
            JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
            WHERE resultado.desempeño = 'ALTO'
            AND evaluacion.nombre LIKE '%Inicial'
            GROUP BY cf_idcurso) AS alto
            ON (medio_bajo.cf_idcurso = alto.cf_idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            GROUP BY cantidadI,promedioI,simceI,ptuI,cantidadMB,promedioMB,simceMB,ptuMB,
            cantidadM,promedioM,simceM,ptuM,cantidadA,promedioA,simceA,ptuA";
                                        
            $result=mysqli_query($mysqli, $sql);
                    
            while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $cantidadI=$row['cantidadI'];
            $promedioI=$row['promedioI'];
            $simceI=$row['simceI'];
            $ptuI=$row['ptuI'];
            $cantidadMB=$row['cantidadMB'];
            $promedioMB=$row['promedioMB'];
            $simceMB=$row['simceMB'];
            $ptuMB=$row['ptuMB'];
            $cantidadM=$row['cantidadM'];
            $promedioM=$row['promedioM'];
            $simceM=$row['simceM'];
            $ptuM=$row['ptuM'];
            $cantidadA=$row['cantidadA'];
            $promedioA=$row['promedioA'];
            $simceA=$row['simceA'];
            $ptuA=$row['ptuA'];
            }
                
            ?>

            <table class="tabla">                   
            <tr><th>Categoría de Desempeño</th><th>Cant. Estudiantes</th><th>Promedio</th><th>Pje. SIMCE aprox.</th><th>Pje. PTU aprox.</th></tr>
            <tr><td>INSUFICIENTE</td><td><?php echo $cantidadI; ?></td><td><?php echo $promedioI; ?></td><td><?php echo $simceI; ?></td><td><?php echo $ptuI; ?></td></tr>
            <tr><td>MEDIO BAJO</td><td><?php echo $cantidadMB; ?></td><td><?php echo $promedioMB; ?></td><td><?php echo $simceMB; ?></td><td><?php echo $ptuMB; ?></td></tr>
            <tr><td>MEDIO ALTO</td><td><?php echo $cantidadM; ?></td><td><?php echo $promedioM; ?></td><td><?php echo $simceM; ?></td><td><?php echo $ptuM; ?></td></tr>
            <tr><td>ADECUADO</td><td><?php echo $cantidadA; ?></td><td><?php echo $promedioA; ?></td><td><?php echo $simceA; ?></td><td><?php echo $ptuA; ?></td></tr>
            </table>
        </div>
        </div>
    </div>

    <!-- LISTADO DE ALUMNOS POR DESEMPEÑO -->
    <div class="card z-depth-0 bordered">
        <div class="acordeon-seleccion card-header" id="headingSeven2">
        <h5 class="mb-0">
            <button class="acordeon btn btn-link collapsed" type="button" data-toggle="collapse"
            data-target="#collapseSeven2" aria-expanded="false" aria-controls="collapseSeven2">
            Listado de Desempeño
            </button>
        </h5>
        </div>
        <div id="collapseSeven2" class="collapse" aria-labelledby="headingSeven2"
        data-parent="#accordionExample275">
        <div class="card-body">
            
            <table class="tabla">
                                    
            <tr>
                <th>NOMBRE</th>
                <th>INSUFICIENTE</th>
                <th>MEDIO BAJO</th>
                <th>MEDIO ALTO</th>
                <th>ALTO</th>
            </tr>

            <?php require_once "conexion.php";

            $sql="SELECT insuficiente.apellidos AS apellidosI, insuficiente.nombre AS nombreI, insuficiente.nota AS notaI
            FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN grafico ON (grafico.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (grafico.cf_idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN (SELECT alumno.cf_idcurso, alumno.apellidos, alumno.nombre, resultado.nota
                FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
                JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
                WHERE resultado.desempeño = 'INSUFICIENTE'
                AND evaluacion.nombre LIKE '%Inicial') AS insuficiente
            ON (curso.idcurso = insuficiente.cf_idcurso)
            RIGHT JOIN alumno ON (insuficiente.cf_idcurso = alumno.cf_idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            GROUP BY apellidosI, nombreI, notaI";
                                                        
            $result=mysqli_query($mysqli, $sql);

            while( $row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
                $apellidosI=$row['apellidosI'];
                $nombreI=$row['nombreI'];
                $notaI=$row['notaI'];
            ?>
                                    
            <tr>
                <td><?php echo $apellidosI; ?> <?php echo $nombreI; ?></td>
                <td><?php echo $notaI; ?></td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
                                    
            <?php
                }
            ?>

            <?php require_once "conexion.php";

            $sql="SELECT medio_bajo.apellidos AS apellidosMB, medio_bajo.nombre AS nombreMB, medio_bajo.nota AS notaMB
            FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN grafico ON (grafico.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (grafico.cf_idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN alumno ON (curso.idcurso = alumno.cf_idcurso)
            JOIN (SELECT alumno.cf_idcurso, alumno.apellidos, alumno.nombre, resultado.nota
                FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
                JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
                WHERE resultado.desempeño = 'MEDIO BAJO'
                AND evaluacion.nombre LIKE '%Inicial') AS medio_bajo
            ON (alumno.cf_idcurso = medio_bajo.cf_idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            GROUP BY apellidosMB, nombreMB, notaMB;";
                                                        
            $result=mysqli_query($mysqli, $sql);

            while( $row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
                $apellidosMB=$row['apellidosMB'];
                $nombreMB=$row['nombreMB'];
                $notaMB=$row['notaMB'];
            
            ?>
            
            <tr>
                <td><?php echo $apellidosMB; ?> <?php echo $nombreMB; ?></td>
                <td></td>
                <td><?php echo $notaMB; ?></td>
                <td></td>
                <td></td>
            </tr>

            <?php
                }
            ?>

            <?php require_once "conexion.php";

            $sql="SELECT medio.apellidos AS apellidosM, medio.nombre AS nombreM, medio.nota AS notaM
            FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN grafico ON (grafico.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (grafico.cf_idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN (SELECT alumno.cf_idcurso, alumno.apellidos, alumno.nombre, resultado.nota
                FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
                JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
                WHERE resultado.desempeño = 'MEDIO'
                AND evaluacion.nombre LIKE '%Inicial') AS medio
            ON (curso.idcurso = medio.cf_idcurso)
            RIGHT JOIN alumno ON (medio.cf_idcurso = alumno.cf_idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            GROUP BY apellidosM, nombreM, notaM";
                                                        
            $result=mysqli_query($mysqli, $sql);

            while( $row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
                $apellidosM=$row['apellidosM'];
                $nombreM=$row['nombreM'];
                $notaM=$row['notaM'];
            
            ?>

            <tr>
                <td><?php echo $apellidosM; ?> <?php echo $nombreM; ?></td>
                <td></td>
                <td></td>
                <td><?php echo $notaM; ?></td>
                <td></td>
            </tr>

            <?php
                }
            ?>

            <?php require_once "conexion.php";

            $sql="SELECT alto.apellidos AS apellidosA, alto.nombre AS nombreA, alto.nota AS notaA
            FROM resultado LEFT JOIN evaluacion ON (resultado.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN grafico ON (grafico.cf_idevaluacion = evaluacion.idevaluacion)
            JOIN aplica ON (grafico.cf_idevaluacion = aplica.cf_idevaluacion)
            LEFT JOIN curso ON (aplica.cf_idcurso = curso.idcurso)
            JOIN (SELECT alumno.cf_idcurso, alumno.apellidos, alumno.nombre, resultado.nota
                FROM evaluacion JOIN resultado ON (evaluacion.idevaluacion = resultado.cf_idevaluacion)
                JOIN alumno ON (resultado.cf_idalumno = alumno.idalumno)
                WHERE resultado.desempeño = 'ALTO'
                AND evaluacion.nombre LIKE '%Inicial') AS alto
            ON (curso.idcurso = alto.cf_idcurso)
            RIGHT JOIN alumno ON (alto.cf_idcurso = alumno.cf_idcurso)
            JOIN colegio ON (curso.cf_idcolegio = colegio.idcolegio)
            JOIN encargado ON (colegio.cf_idencargado = encargado.idencargado)
            WHERE encargado.email='$email'
            GROUP BY apellidosA,nombreA,notaA";
                                                        
            $result=mysqli_query($mysqli, $sql);

            while( $row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
                $apellidosA=$row['apellidosA'];
                $nombreA=$row['nombreA'];
                $notaA=$row['notaA'];
            
            ?>

            <tr>
                <td><?php echo $apellidosA; ?> <?php echo $nombreA; ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?php echo $notaA; ?></td>
            </tr>
                                    
            <?php
                }
            ?>
                                
            </table>
        </div>
        </div>
    </div>
    </div>

</section>

<script src="node_modules/jquery/dist/jquery.min.js"></script> 
<script src="node_modules/popper.js/dist/popper.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>

<script type="text/javascript">
    function Opciones(event,habilidad){
        console.log(event.target.value);
        var opcion=event.target.value;

        if(opcion=='FiltroHabilidad'){
            $('#habilidad').val(habilidad);
        }
    }    
</script>
</body>
</html>