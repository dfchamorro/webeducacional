<?php
    include "config.php";

    // Check user login or not
    if(!isset($_SESSION['uname'])){
        header('Location: index.php');
    }

    // logout
    if(isset($_POST['but_logout'])){
        session_destroy();
        header('Location: index.php');
    }
?> 

<!DOCTYPE html> <!-- Para renderizar a los estándares actuales-->
<html lang="en">
<head>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <!-- Referencias Bootstrap -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    
    <!-- Conexión -->
    <?php require_once "conexion.php"; ?> 

    <title>Resultados COA</title>
</head>
    
<body>
    <header>
        <nav class="barra navbar navbar-expand-md navbar-dark">
            <a class="navbar-brand" href="#">Diagnóstico de Objetivos de Aprendizaje</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active"><a href="./index.html" class="nav-link">Home</a></li>
                    <li class="nav-item"><a href="./contacto.html" class="nav-link">Contacto</a></li>
                </ul>

                <ul class="barra navbar-nav ml-auto">
                    <li class="nav-item">
                        <form method="post" action="">
                            <input class="btn-cerrar nav-link navbar-dark bg-dark" type="submit" value="Cerrar Sesión" name="but_logout">
                        </form>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <section class="container">
        <div class="contenedor">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Home</li>
                </ol>
            </nav>
        </div>

        <div class="d-flex flex-wrap">

            <div class="card card-examen d-flex flex-column justify-content-between ml-2">
                <div class="card-body">
                    <h5 class="card-title">Resultados Examen Inicial</h5>
                    <p class="card-text">Muestra los resultados del examen inicial.</p>
                </div>
                <div>
                    <form action="home.php" method="post">
                        <input type="button" id="verInicial" name="verInicial" class="btn-seleccion btn btn-dark btn-reserva" onClick="location.href='inicial.php'" value="Ver">
                    </form>
                </div>
            </div>

            <div class="card card-examen d-flex flex-column justify-content-between ml-2">
                <div class="card-body">
                    <h5 class="card-title">Resultados Examen Intermedio</h5>
                    <p class="card-text">Muestra los resultados del examen intermedio.</p>
                </div>
                <div>
                    <form action="home.php" method="post">
                    <input type="button" id="verIntermedio" name="verIntermedio" class="btn-seleccion btn btn-dark btn-reserva" onClick="location.href='intermedio.php'" value="Ver">
                    </form>
                </div>
            </div>

            <div class="card card-examen d-flex flex-column justify-content-between ml-2">
                <div class="card-body">
                    <h5 class="card-title">Resultados Examen Final</h5>
                    <p class="card-text">Muestra los resultados del examen final.</p>
                </div>
                <div>
                    <form action="home.php" method="post">
                    <input type="button" id="verFinal" name="verFinal" class="btn-seleccion btn btn-dark btn-reserva" onClick="location.href='final.php'" value="Ver">
                    </form>
                </div>
            </div>
        </div>   
    </section>

    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
    <script src="node_modules/jquery/dist/jquery.min.js"></script> 
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>
